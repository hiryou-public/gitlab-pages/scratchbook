.. raw:: html

    <style>
        main {
            width: 950px;
        }

        div.holder {
            padding-left: 0;
            overflow: hidden;
        }
        div.leftside {
            padding: 0px;
            float: left;
        }
        div.rightside {
            float: left;
            width: 400px;
        }

        div.container.blue-box {
            border: 1px solid blue;
            padding-left: 20px;
        }

        div.math-legends li > p > .formula:first-child {
            display: inline-block;
            width: 120px;
            text-align: left;
            margin-top: 0;
            margin-bottom: 0;
        }

        div.formula, div.math {
            font-size: 18px;
            text-align: left;
            padding-left: 35px;
        }
        div.formula span, div.math span {
            font-size: 18px;
        }

        p:has(> span.formula), p:has(> span.math) {
            font-size: 18px;
        }

        span.formula, span.formula span, span.math, span.math span {
            font-size: 18px;
        }
        span.formula > span, span.math > span {
            #margin-right: 4px;
        }

        span.stretchy {
            font-size: 28px;
        }

        div:has(> span.environment.align) {
            padding-left: 35px;
            margin: 0;
            display: inline-block;
        }

        .marker {color:#A0C5AC; float: right}

        .emphaline {
            text-decoration: underline;
            font-style: italic;
        }
    </style>

.. role:: marker
.. role:: emphaline

Welcome to Long's GitLab Pages!
====================================================

----

4.6 Derive Generic Back Propagation
-----------------------------------

We'll derive the back propagation generically for ANN by preserving the term of activation function derivative. Then, we
can plug in any activation func accordingly.

.. container:: holder

   .. container:: leftside

      .. figure:: static/ann-back.png
         :width: 450

   .. container:: rightside

        Neuron names: :math:`A_1, A_2, B_1, ..., Y_2, Y2`

        Computation at each output neuron:
            Net linear sum: :math:`h_{Y_1} = w_{11}b_1 + w_{21}b_2 + w_{31}b_3`

            Activation: :math:`y_1 = g_{Y_1} = g(h_{Y_1})`

        Loss: :math:`E = \sum_{i=1}^N E_i = E_1 + E_2`

2 main driving factors of how the math works out: which loss func & which activation func

.. container:: holder

   .. container:: leftside

        Which loss func:

        * MSE (mean quare error)

            :math:`E_1 = \frac{1}{2} (y_1 - t_1)^2`

            :math:`E_2 = \frac{1}{2} (y_2 - t_2)^2`

        * Cross-entropy error func:

            :math:`E_1 = -t_1 \star ln(y_1)`

            :math:`E_2 = -t_2 \star ln(y_2)`

   .. container:: rightside

        Which activation func:

        * Linear:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = h_{Y_k}`

            => :math:`g\prime (h_{Y_k}) = 1, (g\prime w/\ respect\ to\ h_{Y_k})`

        * Sigmoid:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = \frac{1}{1 + exp(-\beta \star h_{Y_k})}`

            => :math:`g\prime (h_{Y_k}) = y_k (1 - y_k)`

        * Soft-max:

            :math:`y_k = g_{Y_k} = g(h_{Y_k}) = \frac{exp(h_k)}{\sum_{r=1}^N exp(h_r)}`

            => :math:`\frac{\partial g(h_{Y_k})}{\partial h_{Y_r}} = @`
            `Jacobian matrix <https://towardsdatascience.com/derivative-of-the-softmax-function-and-the-categorical-cross-entropy-loss-ffceefc081d1>`_ (:math:`k, r \in 1..N`)

:emphaline:`The essence of measuring affect of any weight` :math:`w_{ij}` on loss `E` started with this derivative chain:

    .. container:: blue-box

        (Example for weight :math:`w_{11}`)

        :math:`\frac{\partial E}{\partial w_{11}} = \frac{\partial E}{\partial h_{Y_1}} \star \frac{\partial h_{Y_1}}{\partial w_{11}}` :marker:`(4.6.1)`

    * Term :math:`\frac{\partial h_{Y_1}}{\partial w_{11}}` is the easiest one. Commonly the net output `h` to any neuron
      is just a linear sum from previous layer & assoc weights, this term becomes just the input scalar from the the
      connected neuron in the previous layer:

        :math:`h_{Y_1} = w_{11} b_1 + b_21 b_2 + w_{31} b_3 + bias`

        => :math:`\frac{\partial h_{Y_1}}{\partial w_{11}} = b1`

    * Strategically :math:`\frac{\partial E}{\partial h_{Y_1}}` is set as a :math:`\delta_{Y_1}` term - the main contributor and
      differentiator among activation & loss funcs in use.

      Once :math:`\delta_{Y_k}` term for an output neuron is computed, :math:`\delta` terms for hidden layer neurons
      (layer n, n-1, n-2, etc) are computed recursively through this chain:

        :math:`\delta_{Y_k}` -> :math:`\delta_{j}^{(n)}` -> :math:`\delta_{i}^{(n-1)}` -> ...

      This is exactly what back-propagation is doing. It's very much like a dynamic programming algorithm.

    * Then a particular weight :math:`w_{ij}` between neuron i-th in layer (n)-th and neuron j-th in layer (n+1)-th is
      smoothen as:

        .. container:: blue-box

            :math:`w_{ij} -= \frac{\partial E}{\partial w_{ij}} = \delta_j^{(n+1)} \star y_i^{(n)}` (replace :math:`y_i^{(n)}` by :math:`x_i` if neuron i-th is in input layer)

    * There's no :math:`\delta` term for input neurons

:emphaline:`For Sigmoid output & MSE loss`: compute :math:`\delta` terms

    Back-propagation from output layer:

    .. math::

        \delta_{Y_1} &= \frac{\partial E}{\partial h_{Y_1}} = \frac{\partial (E_1 + E_2)}{\partial h_{Y_1}} & \\
                     &= \frac{\partial E_1}{\partial h_{Y_1}} \ (because\ sigmoid\ neuron\ outputs\ are\ independent\ from\ one\ another,\ only\ 1\ E\ term\ applies) & \\
                     &= \left( \frac{\partial E_1}{\partial y_1} \right) \star \left( \frac{\partial y_1}{\partial h_{Y_1}} \right)  & \\
                     &= \left( \frac{\partial \left( \frac{1}{2} (y_1 - t_1)^2 \right)}{\partial y_1} \right) \star sigmoid\prime_{Y_1} & \\
        \delta_{Y_1} &= (y_1 - t_1) \star y_1(1 - y_1) &

    => back-propagation from an output neuron :math:`Y_k, k \in 1...N`:

        .. container:: blue-box

            :emphaline:`for sigmoid/MSE loss`:
            :math:`\delta_{Y_k} = \frac{\partial E}{\partial h_{Y_k}} = (y_k - t_k) \star y_k(1 - y_k)` :marker:`(* 4.6.1)`

:emphaline:`For Soft-max output & Cross-entropy loss (ln)`: compute :math:`\delta` terms
(helping sources: `crappy <https://alexcpn.github.io/html/NN/ml/8_backpropogation_full/>`_, `stackexchange <https://stats.stackexchange.com/questions/235528/backpropagation-with-softmax-cross-entropy>`_)

    Back-propagation from output layer:

    .. math::

        \delta_{Y_1} &= \frac{\partial E}{\partial h_{Y_1}} = \frac{\partial (E_1 + E_2)}{\partial h_{Y_1}} \ (all\ E\ terms\ included\ because\ soft-max\ neuron\ outputs\ affect\ one\ another\ due\ to\ the\ common\ denominator) \\
                     &= - \frac{\partial [t_1 \star ln(y_1) + t_2 \star ln(y_2)]}{\partial h_{Y_1}} \\
                     &= - \left( t_1 \frac{\partial ln(y_1)}{\partial h_{Y_1}} + t_2 \frac{\partial ln(y_2)}{\partial h_{Y_1}} \right) \\
                     &= - \left( t_1 \star \frac{1}{y_1} \star \frac{\partial y_1}{\partial h_{Y_1}} + t_2 \star \frac{1}{y_2} \star \frac{\partial y_2}{\partial h_{Y_1}} \right)

    , here recalled that:

        :math:`\frac{\partial y_1}{\partial h_{Y_1}}`: derivative of soft-max where `Kronecker delta <https://en.wikipedia.org/wiki/Kronecker_delta>`_ :math:`\delta_ij = 1` (because i=j=1)

        :math:`\frac{\partial y_2}{\partial h_{Y_1}}`: derivative of soft-max where Kronecker delta :math:`\delta_ij = 0` (because i=2 != j=1)

    , therefore:

    .. math::

        \delta_{Y_1} &= - \left( t_1 \star \frac{1}{y_1} \star \frac{\partial y_1}{\partial h_{Y_1}} + t_2 \star \frac{1}{y_2} \star \frac{\partial y_2}{\partial h_{Y_1}} \right) \\
                     &= - \left( t_1 \star \frac{1}{y_1} \star y_1(1 - y_1) \right) - \left( t_2 \star \frac{1}{y_2} \star y_2(0 - y_1) \right) \\
                     &= - t_1 (1 - y_1) - t_2 (0 - y_1) = -t_1 + t_1 \star y_1 + t_2 \star y_1 \\
                     &= -t_1 + (t_1 + t_2) y_1 & (assuming\ 1-hot-encoding\ gives: t_1 + t_2 = 1) \\
        \delta_{Y_1} &= y_1 - t_1

    => back-propagation from an output neuron :math:`Y_k, k \in 1...N`:

        .. container:: blue-box

            :emphaline:`for soft-max/cross-entropy loss`:
            :math:`\delta_{Y_k} = \frac{\partial E}{\partial h_{Y_k}} = y_k - t_k` :marker:`(* 4.6.1)`

:emphaline:`Back-propagation from hidden layers, for either sigmoid/MSE or soft-max/cross-entropy`:

    :math:`\delta` terms for hidden layers are typically the same among ANNs regardless of what output activation & LOSS
    func in use. Reason is because once the algorithm reaches hidden layers, their :math:`\delta` terms no longer depend
    directly on those funcs. Instead, these terms are computed recursively from `\delta`'s of the an immediate layer
    to its right. As :math:`\delta` terms in the output layer was ready, they propagate back all the way to the beginning
    of the network.

    .. math::

        \delta_{B_1} &= \frac{\partial E}{\partial h_{B_1}} = \left[ \frac{\partial E}{\partial b_1} \right] \star \left( \frac{\partial b_1}{\partial h_{B_1}} \right) & \\
                     &= \left[ \frac{\partial E}{\partial h_{Y_1}} \star \frac{\partial h_{Y_1}}{\partial b_1} + \frac{\partial E}{\partial h_{Y_2}} \star \frac{\partial h_{Y_2}}{\partial b_1} \right] \star \left( \frac{\partial b_1}{\partial h_{B_1}} \right) & \\
                     &= [ \delta_{Y_1} \star f_{b_1}\prime (w_{11} b_1 + w_{21} b_2 + w_{31} b_3) \ +\  \delta_{Y_2} \star f_{b_1}\prime (w_{12} b_1 + w_{22} b_2 + w_{32} b_3) ] \star \frac{\partial b_1}{\partial h_{B_1}} \\
        \delta_{B_1} &= [ \delta_{Y_1} \star w_{11} + \delta_{Y_2} \star w_{12} ] \star \frac{\partial b_1}{\partial h_{B_1}}

    => back-propagation from a hidden neuron i-th in layer (n)-th depends on 2 things:

        * ALL N :math:`\delta` terms in layer (n+1)-th with respective weights from neuron i-th to this layer

        * Activation function used at neuron i-th in layer (n)-th

        .. container:: blue-box

            :emphaline:`for hidden layers`:
            :math:`\delta_i^{(n)} = \left[ \sum_{j=1}^N \delta_j^{(n+1)} w_{ij} \right] \star \frac{\partial g_i^{(n)}}{\partial h_i^{(n)}}`

----

The back-propagation algorithm can be seen like a dynamic programming iteration starting from the output layer. Every
neuron except input ones have a :math:`\delta` term to be calculated. After all these :math:`\delta` terms are ready,
weight update between layers can be performed.

:emphaline:`Legends`:

.. container:: math-legends

    * :math:`h_k^{(out)}`   linear sum (from previous neurons & assoc weights) at neuron k-th in output layer

    * :math:`y_k, t_k`      ``predicted value`` and ``actual value (target)`` respectively at neuron k-th in output layer

    * :math:`g\prime (h_k^{(out)})`     derivative of activation func w/ respect to :math:`h_k^{(out)}` at neuron k-th in output layer

    * :math:`h_j^{(n)}`     linear sum (from previous neurons & assoc weights) at neuron j-th in layer (n)-th

    * :math:`g\prime (h_j^{(n)})`       derivative of activation func w/ respect to :math:`h_j^{(n)}` at neuron j-th in layer (n)-th

    * :math:`x_i^{(n)}`     output (activation func result) at neuron i-th in layer (n)-th

----

asdf
====

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Fork this project
==================

* https://gitlab.com/pages/sphinx
